package com.hi;

import java.awt.Button;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex12 extends Frame implements ActionListener{
	Button btn1, btn2;
	public Ex12(){
		Panel p=new Panel();
		p.setLayout(new GridLayout(1,2));
		btn1=new Button("시작");
		btn2=new Button("종료");
		btn1.addActionListener(this);
		btn2.addActionListener(this);
		p.add(btn1);
		p.add(btn2);
		add(p);
		setBounds(100-1920,100,300,200);
		setVisible(true);
	}

	public static void main(String[] args) {
		new Ex12();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Button btn=(Button) e.getSource();
		if(btn==btn1){
			System.out.println("시작버튼 클릭");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}else if(btn==btn2){
			System.out.println("종료 버튼 클릭");
		}
	}

}











