package com.hi;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Ex05 {

	public static void main(String[] args) {
		File file=new File("test04.bin");
		int su1;
		double su2;
		char su3;
		boolean boo;
		String msg;
		try {
			FileInputStream fis=new FileInputStream(file);
			DataInputStream dis=new DataInputStream(fis);
			int su=dis.read();
			System.out.println(su);
			su2=dis.readDouble();
			su1=dis.readInt();
			su3=dis.readChar();
			boo=dis.readBoolean();
			msg=dis.readUTF();
			System.out.println(su1);
			System.out.println(su2);
			System.out.println(su3);
			System.out.println(boo);
			System.out.println(msg);
			dis.close();
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
