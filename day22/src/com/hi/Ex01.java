package com.hi;

import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.CheckboxMenuItem;
import java.awt.Choice;
import java.awt.Frame;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;

public class Ex01 extends Frame implements ItemListener{
	Choice cho;
	List list;
	Checkbox cb1,cb2,cb3;
	
	public Ex01(){
		Panel p=new Panel();
		
		list=new List(4,true);
		list.add("item1");
		list.add("item2");
		list.add("item3");
		list.add("item4");
		list.addItemListener(this);
//		p.add(list);
		
		cho=new Choice();
		cho.addItem("item1");
		cho.addItem("item2");
		cho.addItem("item3");
		cho.addItem("item4");
		cho.addItemListener(this);
//		p.add(cho);
		CheckboxGroup cbg=new CheckboxGroup();
		cb1=new Checkbox("item1",false,cbg);
		cb1.addItemListener(this);
		p.add(cb1);
		
		cb2=new Checkbox("item2",true,cbg);
		cb2.addItemListener(this);
		p.add(cb2);
		
		cb3=new Checkbox("item3",false,cbg);
		cb3.addItemListener(this);
		p.add(cb3);
		
		add(p);
		setBounds(100-1920,100,500,400);
		setVisible(true);
	}

	public static void main(String[] args) {
		new Ex01();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		System.out.println("cb1:"+cb1.getState());
		System.out.println("cb2:"+cb2.getState());
		System.out.println("cb3:"+cb3.getState());
//		System.out.println(list.getSelectedIndex());
//		System.out.println(list.getSelectedItem());
//		int[] arr1=list.getSelectedIndexes();
//		String[] arr2=list.getSelectedItems();
//		System.out.println(Arrays.toString(arr1));
//		System.out.println(Arrays.toString(arr2));
	}

}
