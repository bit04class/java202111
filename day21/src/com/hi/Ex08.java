package com.hi;

import javax.swing.JFrame;

public class Ex08 extends JFrame {
	
	public Ex08(){
	// DO_NOTHING_ON_CLOSE == 0
	// HIDE_ON_CLOSE       == 1
    // DISPOSE_ON_CLOSE    == 2
    // EXIT_ON_CLOSE       == 3
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(100-1920,100,400,300);
		setVisible(true);
	}

	public static void main(String[] args) {
		new Ex08();
	}

}
