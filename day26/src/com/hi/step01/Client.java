package com.hi.step01;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;

public class Client {

	public static void main(String[] args) {
		String ip="127.0.0.1";
		int port=5000;		// 
		Socket sock=null;
		InputStream is=null;
		InputStreamReader isr=null;
		try {
			sock=new Socket(ip,port);
			is=sock.getInputStream();
			isr=new InputStreamReader(is);
			int su=-1;
			while((su=isr.read())!=-1){
				System.out.print((char)su);
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				isr.close();
				is.close();
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
