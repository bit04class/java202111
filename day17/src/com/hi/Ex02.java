package com.hi;

interface Ball{
	void play();
}

class Baseball implements Ball{
	public void play(){
		System.out.println("던지고 논다");
	}
}
class BowlingBall implements Ball{
	public void play(){
		System.out.println("굴리고 논다");
	}
}
class Box<T>{
	T obj;
	public void setObj(T obj){
		this.obj=obj;
	}
	public T getObj(){
		return this.obj;
	}
}

public class Ex02 {

	public static void main(String[] args) {
		Box<Ball> box=new Box<Ball>();
		Baseball ball1=new Baseball();
		BowlingBall ball2=new BowlingBall();
		
		box.setObj(ball2);
		Ball obj=box.getObj();
		obj.play();
	}

}










