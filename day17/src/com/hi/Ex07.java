package com.hi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class Ex07 {

	public static void main(String[] args) {
		ArrayList<Integer> list=new ArrayList<>();
		list.add(2222);
		list.add(1111);
		list.add(3333);
		list.add(1111);
		HashSet<Integer> set=new HashSet<>();
		for(int i=0; i<list.size(); i++){
			set.add(list.get(i));
		}
		System.out.println("---------------------------");
//		HashSet<Integer> set=new HashSet<>(list);
		set.addAll(list);
		Iterator<Integer> ite=set.iterator();
		while(ite.hasNext()){
			int temp=ite.next();
			System.out.println(temp);
		}
	}

}











