package com.hi;

public class Ex02 implements Runnable{

	public static void main(String[] args) {
		Ex02 work=new Ex02();
		Thread thr=new Thread(work);
		thr.start();

	}

	@Override
	public void run() {
		Thread me=Thread.currentThread();
		String name=me.getName();
		System.out.println(name+" 스레드로 하고자 하는 일");
	}

}
