package com.hi;

public class Ex11 {

	public static void main(String[] args) {
		// 예외처리
		
		int[] arr={1,3,5};

		try{
			System.out.println(arr[0]);
			System.out.println(arr[1]);
			System.out.println(arr[2]);
			System.out.println(arr[3]);
			System.out.println(arr[2]);
			System.out.println(arr[1]);
			System.out.println(arr[0]);
		}catch(java.lang.ArrayIndexOutOfBoundsException e){
			//오류가 났을때의 상황
			
		}
		System.out.println("main end...");
	}

}
