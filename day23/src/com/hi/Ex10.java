package com.hi;

import java.awt.Frame;
import java.awt.Label;

public class Ex10 extends Frame {
	static Label la;
	
	public Ex10(){
		la=new Label();
		la.setAlignment(Label.CENTER);
		add(la);
		setBounds(100-1920,100,200,100);
		setVisible(true);
	}

	public static void main(String[] args) {
		Ex10 me = new Ex10();
		for(int i=10; i>=0; i--){
			la.setText(""+i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
