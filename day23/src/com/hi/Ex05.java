package com.hi;

public class Ex05 extends Thread {

	public static void main(String[] args) {
		Ex05 thr1=new Ex05();
		Ex05 thr2=new Ex05();
		Ex05 thr3=new Ex05();
		thr1.setPriority(Thread.MIN_PRIORITY); // thread �켱���� 1~10 ,default=5
		thr2.setPriority(Thread.NORM_PRIORITY);
		thr3.setPriority(Thread.MAX_PRIORITY);
		thr1.start();
		thr2.start();
		thr3.start();

	}
	
	@Override
	public void run() {
		int su=this.getPriority();
		for(int i=0; i<10; i++){
			System.out.println(su+getName()+" run..."+i);
		}
	}

}
