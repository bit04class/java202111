package com.hi;

public class Ex08 {

	public static void main(String[] args){
		System.out.println(args.length);
		// io -> input를 쉽게 해주는 유틸
		java.util.Scanner sc=null;
		sc=new java.util.Scanner("1111\n한글1\n2222\n한글2");
//		System.out.println(sc.next());		// \t\r\n 
//		System.out.println(sc.nextLine());	//\n 
//		System.out.println(sc.nextLine());	//\n 
//		System.out.println(sc.nextInt()+1);	//\n 
		while(sc.hasNext()){
			System.out.print(sc.nextLine());
		}
	}

}
