package com.hi;

public class Ex02 {

	public static void main(String[] args) {
//		배열함수 목록을 반복문으로 늘릴 수 있나요?
//
//				kor[0]={0};
//				kor[1]={0};
//				.
//				.
//				.
//				kor[반복된수]={0};
//				>> kor[]={0,0, .... ,0};을 반복문으로 만들고 싶은데 안되네요
		int[] arr=new int[5];
		for(int i=0; i<arr.length; i++){
			arr[i]=1;
		}
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}

	}

}
