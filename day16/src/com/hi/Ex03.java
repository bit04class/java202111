package com.hi;

import java.util.Date;

public class Ex03 {

	public static void main(String[] args) {
		java.util.Date cal=new Date();
		cal.setMonth(11-1);
		cal.setDate(29);
		cal.setHours(9);
		cal.setMinutes(30);
		System.out.println(cal);
		System.out.println(cal.getYear()+1900);
		System.out.println(cal.getMonth()+1);
		System.out.println(cal.getDate());
		System.out.println(cal.getHours());
		System.out.println(cal.getMinutes());
		System.out.println(cal.getSeconds());
		System.out.println((System.currentTimeMillis()-cal.getTime())/1000/60/60/24);

	}

}
