package com.hi;

public class Ex03 {

	public static void main(String[] args) {
		String msg1="abcd";
		String msg2="abcabcda";
		String msg3="";
		String msg4="AaBbCcDd";
		String msg5="           java web    framwork	db         ";
		
		System.out.println(msg1.contains("bC"));
		System.out.println(msg1.startsWith("ab"));
		System.out.println(msg1.startsWith("AB"));
		System.out.println(msg1.endsWith("cd"));
		System.out.println(msg1.endsWith("cde"));
		System.out.println(msg3.length()==0);
		System.out.println(msg3.isEmpty());
		System.out.println(msg2.lastIndexOf('a',6));
		System.out.println(msg4.toUpperCase());
		System.out.println(msg4.toLowerCase());
		System.out.println(">>>"+msg5.trim()+"<<<");
		
	}

}
