package com.hi;

import java.util.ArrayList;

public class Ex01 {

	public static void main(String[] args) {
		// List(ArrayList), Set(HashSet), Map(HashMap)
		// 제네릭 ~1.5
//		ArrayList<String> list=new ArrayList<String>();
		// 추론타입(1.7~)
		ArrayList<String> list=new ArrayList<>();
		list.add("첫번째");
		list.add("두번째");
		list.add("세번째");
		list.add("네번째");
		
		for(int i=0; i<list.size(); i++){
			String msg=list.get(i);
			System.out.println(msg);
		}
		
		
	}

}
