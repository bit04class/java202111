package com.hi;

public class Ex06 {
	public int su;
	public String name;
	
	public Ex06(){}
	
	public Ex06(int su,String name){
		this.su=su;
		this.name=name;
	}

	public static void main(String[] args) {
		int[] arr1=new int[4];
		arr1=new int[2];
		for(int i=0; i<arr1.length; i++){
			System.out.println(arr1[i]);
		}

		String[] arr2=new String[3];
		for(int i=0; i<arr2.length; i++){
			System.out.println(arr2[i]);
		}
		System.out.println("---------------------------");
		Ex06 me1=new Ex06(1111,"첫번째");
		Ex06 me2=new Ex06();
		me2.su=2222;
		me2.name="두번째";
		Ex06 me3=new Ex06(3333,"세번째");
		
		Ex06[] arr3=new Ex06[3];
		arr3[0]=me1;
		arr3[1]=me2;
		arr3[2]=me3;
		
		for(int i=0; i<arr3.length; i++){
			Ex06 me=arr3[i];
			System.out.println(me.su);
			System.out.println(me.name);
		}
		System.out.println("---------------------------");
		// 다중배열(2차원 배열...)
		int[] temp=new int[]{1,2,7};
		int[][] arr4;
		arr4=new int[3][];
		
		arr4[0]=temp;
		arr4[1]=new int[]{3,4};
		arr4[2]=new int[]{5,6,8,9};
		
//		arr4[0][2]=10;
		
		for(int i=0; i<arr4.length; i++){
			for(int j=0; j<arr4[i].length; j++){
				System.out.print(arr4[i][j]+" ");
			}
			System.out.println();
		}
	}

}


























